#!/usr/bin/env sh

cd api

proto_paths="protos/**/*.proto"

set -x

docker run --rm \
  -v $(pwd)/protos:/protos \
  -v $(pwd):/out \
  pseudomuto/protoc-gen-doc \
  --doc_opt=markdown,grpc-api.md \
  $proto_paths



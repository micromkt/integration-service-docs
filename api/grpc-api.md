# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [charitysuite/address_value.proto](#charitysuite/address_value.proto)
    - [AddressValue](#charitysuite.AddressValue)
  
- [charitysuite/area_entity.proto](#charitysuite/area_entity.proto)
    - [AreaEntity](#charitysuite.AreaEntity)
  
- [charitysuite/contact_value.proto](#charitysuite/contact_value.proto)
    - [ContactValue](#charitysuite.ContactValue)
  
- [charitysuite/echo_service.proto](#charitysuite/echo_service.proto)
    - [EchoCredentialsRequest](#charitysuite.EchoCredentialsRequest)
    - [EchoCredentialsResponse](#charitysuite.EchoCredentialsResponse)
    - [EchoGatherResponse](#charitysuite.EchoGatherResponse)
    - [EchoMessage](#charitysuite.EchoMessage)
    - [EchoNRequestMessage](#charitysuite.EchoNRequestMessage)
  
    - [EchoService](#charitysuite.EchoService)
  
- [charitysuite/entity_id.proto](#charitysuite/entity_id.proto)
    - [EntityId](#charitysuite.EntityId)
  
- [charitysuite/error_value.proto](#charitysuite/error_value.proto)
    - [ErrorValue](#charitysuite.ErrorValue)
    - [ResponseStatus](#charitysuite.ResponseStatus)
  
- [charitysuite/location_entity.proto](#charitysuite/location_entity.proto)
    - [LocationEntity](#charitysuite.LocationEntity)
  
- [charitysuite/member_entity.proto](#charitysuite/member_entity.proto)
    - [EmailAccountDetails](#charitysuite.EmailAccountDetails)
    - [MemberEntity](#charitysuite.MemberEntity)
    - [MicrosoftAccountDetails](#charitysuite.MicrosoftAccountDetails)
  
- [charitysuite/member_role.proto](#charitysuite/member_role.proto)
    - [MemberRole](#charitysuite.MemberRole)
  
    - [MemberRoleName](#charitysuite.MemberRoleName)
    - [MemberRoleScope](#charitysuite.MemberRoleScope)
  
- [charitysuite/member_service.proto](#charitysuite/member_service.proto)
    - [CreateMemberRequest](#charitysuite.CreateMemberRequest)
    - [CreateMemberResponse](#charitysuite.CreateMemberResponse)
    - [DeleteMemberRequest](#charitysuite.DeleteMemberRequest)
    - [DeleteMemberResponse](#charitysuite.DeleteMemberResponse)
    - [GetMemberRequest](#charitysuite.GetMemberRequest)
    - [GetMemberResponse](#charitysuite.GetMemberResponse)
    - [ListMembersRequest](#charitysuite.ListMembersRequest)
    - [ListMembersResponse](#charitysuite.ListMembersResponse)
    - [UpdateMemberRequest](#charitysuite.UpdateMemberRequest)
    - [UpdateMemberRequest.UpdateAddressRequest](#charitysuite.UpdateMemberRequest.UpdateAddressRequest)
    - [UpdateMemberRequest.UpdateContactRequest](#charitysuite.UpdateMemberRequest.UpdateContactRequest)
    - [UpdateMemberRequest.UpdateEmailPasswordRequest](#charitysuite.UpdateMemberRequest.UpdateEmailPasswordRequest)
    - [UpdateMemberRequest.UpdateMemberRolesRequest](#charitysuite.UpdateMemberRequest.UpdateMemberRolesRequest)
    - [UpdateMemberRequest.UpdatePINRequest](#charitysuite.UpdateMemberRequest.UpdatePINRequest)
    - [UpdateMemberResponse](#charitysuite.UpdateMemberResponse)
  
    - [MemberService](#charitysuite.MemberService)
  
- [charitysuite/organisation_entity.proto](#charitysuite/organisation_entity.proto)
    - [OrganisationEntity](#charitysuite.OrganisationEntity)
  
- [charitysuite/organisation_service.proto](#charitysuite/organisation_service.proto)
    - [ListAreasRequest](#charitysuite.ListAreasRequest)
    - [ListAreasResponse](#charitysuite.ListAreasResponse)
    - [ListLocationsRequest](#charitysuite.ListLocationsRequest)
    - [ListLocationsResponse](#charitysuite.ListLocationsResponse)
    - [ListOrganisationsRequest](#charitysuite.ListOrganisationsRequest)
    - [ListOrganisationsResponse](#charitysuite.ListOrganisationsResponse)
    - [ListRegionsRequest](#charitysuite.ListRegionsRequest)
    - [ListRegionsResponse](#charitysuite.ListRegionsResponse)
  
    - [OrganisationService](#charitysuite.OrganisationService)
  
- [charitysuite/references.proto](#charitysuite/references.proto)
    - [OrgEntityReference](#charitysuite.OrgEntityReference)
    - [Reference](#charitysuite.Reference)
  
- [charitysuite/region_entity.proto](#charitysuite/region_entity.proto)
    - [RegionEntity](#charitysuite.RegionEntity)
  
- [charitysuite/status_service.proto](#charitysuite/status_service.proto)
    - [GetStatusRequest](#charitysuite.GetStatusRequest)
    - [GetStatusResponse](#charitysuite.GetStatusResponse)
  
    - [StatusService](#charitysuite.StatusService)
  
- [charitysuite/user_account_value.proto](#charitysuite/user_account_value.proto)
    - [UserAccountValue](#charitysuite.UserAccountValue)
  
    - [ProviderId](#charitysuite.ProviderId)
  
- [Scalar Value Types](#scalar-value-types)



<a name="charitysuite/address_value.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/address_value.proto



<a name="charitysuite.AddressValue"></a>

### AddressValue
Describes an Address


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| building_name_number | [string](#string) |  | Building name or number |
| line_1 | [string](#string) |  | Line 1 of Address |
| line_2 | [string](#string) |  | Line 2 of Address |
| post_town | [string](#string) |  | Postal town |
| postcode | [string](#string) |  | Post code |





 

 

 

 



<a name="charitysuite/area_entity.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/area_entity.proto



<a name="charitysuite.AreaEntity"></a>

### AreaEntity
An area.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_id | [EntityId](#charitysuite.EntityId) |  | EntityId of the containing organisation |
| entity_id | [EntityId](#charitysuite.EntityId) |  | EntityId of this instance within its type (for this record - same as org_id) |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| contact | [ContactValue](#charitysuite.ContactValue) |  | Contact Details |
| address | [AddressValue](#charitysuite.AddressValue) |  | Address Details |
| entity_id_of_region | [EntityId](#charitysuite.EntityId) |  | EntityId of the region this area is part of |





 

 

 

 



<a name="charitysuite/contact_value.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/contact_value.proto



<a name="charitysuite.ContactValue"></a>

### ContactValue
Describes a Contact


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| display_name | [string](#string) |  | Display name.

If this is not set then it will be calculated from other name values. If set then this will be used where a display name is required. |
| first_name | [string](#string) |  | First name |
| last_name | [string](#string) |  | Last or Surname |
| title | [string](#string) |  | Title |
| email | [string](#string) |  | Contact email address |
| phone_number | [string](#string) |  | Primary phone number |
| alternate_phone_number | [string](#string) |  | Alternate phone number |





 

 

 

 



<a name="charitysuite/echo_service.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/echo_service.proto
The EchoService feature is provided to enable clients to set up and test authentication and connectivity
using gRPC messages/streams from their applications.
It is not intended to be used as part of a live application.


<a name="charitysuite.EchoCredentialsRequest"></a>

### EchoCredentialsRequest







<a name="charitysuite.EchoCredentialsResponse"></a>

### EchoCredentialsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| decoded_credentials | [string](#string) |  |  |
| user_id | [string](#string) |  |  |






<a name="charitysuite.EchoGatherResponse"></a>

### EchoGatherResponse
Response for gathering up a stream of messages


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| messages | [EchoMessage](#charitysuite.EchoMessage) | repeated |  |






<a name="charitysuite.EchoMessage"></a>

### EchoMessage
Simple message wrapping a text field


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| text | [string](#string) |  |  |






<a name="charitysuite.EchoNRequestMessage"></a>

### EchoNRequestMessage
Request for echoing back N x message


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| message | [EchoMessage](#charitysuite.EchoMessage) |  |  |
| number_of_messages | [int32](#int32) |  |  |





 

 

 


<a name="charitysuite.EchoService"></a>

### EchoService
The EchoService provides various endpoints useful for testing connetivity.

NOT INTENDED FOR PRODUCTION USE

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| EchoSingle | [EchoMessage](#charitysuite.EchoMessage) | [EchoMessage](#charitysuite.EchoMessage) | Send and receive a single textual message (unary - unary) |
| EchoN | [EchoNRequestMessage](#charitysuite.EchoNRequestMessage) | [EchoMessage](#charitysuite.EchoMessage) stream | Send a message and a multiplier N and receive back a stream of N x message. (unary - stream) |
| EchoGather | [EchoMessage](#charitysuite.EchoMessage) stream | [EchoGatherResponse](#charitysuite.EchoGatherResponse) | Send a stream of messages and return all the messages gathered together as a single message). (stream - unary) |
| EchoBidi | [EchoMessage](#charitysuite.EchoMessage) stream | [EchoMessage](#charitysuite.EchoMessage) stream | Send and receive a bi-directional stream of messages. (stream - stream) |
| EchoCredentials | [EchoCredentialsRequest](#charitysuite.EchoCredentialsRequest) | [EchoCredentialsResponse](#charitysuite.EchoCredentialsResponse) | Sends back details of decoded credentials from the request message. for debug purposes |

 



<a name="charitysuite/entity_id.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/entity_id.proto
Id Types


<a name="charitysuite.EntityId"></a>

### EntityId
An id for an entity will consist of an `internal_id` and optionally an `external_id`


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| internal_id | [string](#string) |  | An Entity will always have an internally assigned identifier |
| external_id | [string](#string) |  | An Entity can be assigned an external id at creation time that can also be used in references |





 

 

 

 



<a name="charitysuite/error_value.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/error_value.proto



<a name="charitysuite.ErrorValue"></a>

### ErrorValue
An individual error.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| http_status | [string](#string) |  | The HTTP status code applicable to this problem, expressed as a string value. |
| code | [string](#string) |  | an application-specific error code, expressed as a string value. |
| title | [string](#string) |  | a short, human-readable summary of the problem |
| detail | [string](#string) |  | a human-readable explanation specific to this occurrence of the problem. |






<a name="charitysuite.ResponseStatus"></a>

### ResponseStatus
This message indicates the overall status of a response using HTTP status codes and a nested
array of error objects if relevant.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| http_status | [string](#string) |  | The HTTP status code that applies to a whole response |
| errors | [ErrorValue](#charitysuite.ErrorValue) | repeated | Specific Errors related to this response if relevant |





 

 

 

 



<a name="charitysuite/location_entity.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/location_entity.proto



<a name="charitysuite.LocationEntity"></a>

### LocationEntity
An location.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_id | [EntityId](#charitysuite.EntityId) |  | EntityId of the containing organisation |
| entity_id | [EntityId](#charitysuite.EntityId) |  | EntityId of this instance within its type (for this record - same as org_id) |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| contact | [ContactValue](#charitysuite.ContactValue) |  | Contact Details |
| address | [AddressValue](#charitysuite.AddressValue) |  | Address Details |
| entity_id_of_area | [EntityId](#charitysuite.EntityId) |  | EntityId of the area this location is part of |





 

 

 

 



<a name="charitysuite/member_entity.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/member_entity.proto



<a name="charitysuite.EmailAccountDetails"></a>

### EmailAccountDetails
Specifies that this member is to be associated with an email/password account.
An account will be created if an existing one is not found.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | Existing userId if known, otherwise one will be assigned |
| email | [string](#string) |  | Email of existing account or new account |
| password | [string](#string) |  | Password |






<a name="charitysuite.MemberEntity"></a>

### MemberEntity
A Member of an organisation.

A Member can have roles to perform actions within CharitySuite
and can have an associated user account to allow logging in.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_id | [EntityId](#charitysuite.EntityId) |  | EntityId of the containing organisation |
| entity_id | [EntityId](#charitysuite.EntityId) |  | EntityId of this instance within its type |
| contact | [ContactValue](#charitysuite.ContactValue) |  | Contact Details |
| address | [AddressValue](#charitysuite.AddressValue) |  | Address Details |
| roles | [MemberRole](#charitysuite.MemberRole) | repeated | The roles assigned to this Member |
| user_accounts | [UserAccountValue](#charitysuite.UserAccountValue) | repeated | The user accounts associated with this Member |
| email_account | [EmailAccountDetails](#charitysuite.EmailAccountDetails) |  | Specifies that this member is to be associated with an email/password account. An account will be created if an existing one is not found. |
| microsoft_account | [MicrosoftAccountDetails](#charitysuite.MicrosoftAccountDetails) |  | Specifies that this member is to be associated with a Microsoft account. When the user with matching details first logs in they will be linked to this Member |
| tillLoginPIN | [string](#string) |  | A PIN Number that can be used by a Member in certain circumstances to provide quick access to a Till Point in place of a full login. |






<a name="charitysuite.MicrosoftAccountDetails"></a>

### MicrosoftAccountDetails
Specifies that this member is to be associated with a Microsoft account.
When the user with matching details first logs in they will be linked to this Member


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | Microsoft user id if known |
| email | [string](#string) |  | Email / username of the associated microsoft account |
| tenant_id | [string](#string) |  | Tenant Id of provider if known |





 

 

 

 



<a name="charitysuite/member_role.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/member_role.proto



<a name="charitysuite.MemberRole"></a>

### MemberRole
Describes a Members role within an organisation


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| role | [MemberRoleName](#charitysuite.MemberRoleName) |  | The name of the role to be applied |
| scope | [MemberRoleScope](#charitysuite.MemberRoleScope) |  | The scope at which the role is to be applied |
| location_id | [Reference](#charitysuite.Reference) |  |  |
| area_id | [Reference](#charitysuite.Reference) |  |  |
| region_id | [Reference](#charitysuite.Reference) |  |  |





 


<a name="charitysuite.MemberRoleName"></a>

### MemberRoleName
Names of the a available roles

| Name | Number | Description |
| ---- | ------ | ----------- |
| MEMBER_ROLE_UNDEFINED | 0 | Default undefined value |
| MEMBER_ROLE_ADMIN | 1 | Administrator Role |
| MEMBER_ROLE_MANAGER | 2 | Manager Role |
| MEMBER_ROLE_CASHIER | 3 | Cashier Role |
| MEMBER_ROLE_USER | 4 | Generate User Role |



<a name="charitysuite.MemberRoleScope"></a>

### MemberRoleScope
Names for the scopes at which roles can be applied

| Name | Number | Description |
| ---- | ------ | ----------- |
| MEMBER_ROLE_SCOPE_UNDEFINED | 0 | Default undefined value |
| MEMBER_ROLE_SCOPE_ORG | 1 | Organisation wide scope |
| MEMBER_ROLE_SCOPE_REGION | 2 | Regional scope |
| MEMBER_ROLE_SCOPE_AREA | 3 | Area scope |
| MEMBER_ROLE_SCOPE_LOCATION | 4 | Location scope |


 

 

 



<a name="charitysuite/member_service.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/member_service.proto
MemberService and related message types.


<a name="charitysuite.CreateMemberRequest"></a>

### CreateMemberRequest
Request to Create a new member


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_ref | [Reference](#charitysuite.Reference) |  | Reference to the organisation this message applies to |
| external_id | [string](#string) |  | External identifier (Optional) |
| contact | [ContactValue](#charitysuite.ContactValue) |  | Contact details (Optional) |
| address | [AddressValue](#charitysuite.AddressValue) |  | Address details (Optional) |
| roles | [MemberRole](#charitysuite.MemberRole) | repeated | Roles for this Member (Optional) |
| email_account | [EmailAccountDetails](#charitysuite.EmailAccountDetails) |  | Specifies that this member is to be associated with an email/password account. An account will be created if an existing one is not found. |
| microsoft_account | [MicrosoftAccountDetails](#charitysuite.MicrosoftAccountDetails) |  | Specifies that this member is to be associated with a Microsoft account. When the user with matching details first logs in they will be linked to this Member |






<a name="charitysuite.CreateMemberResponse"></a>

### CreateMemberResponse
Response to CreateMemberRequest


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| member_ref | [OrgEntityReference](#charitysuite.OrgEntityReference) |  | Reference to the entity this message applies to |






<a name="charitysuite.DeleteMemberRequest"></a>

### DeleteMemberRequest
Request to delete a MemberEntity


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| member_ref | [OrgEntityReference](#charitysuite.OrgEntityReference) |  | Reference to the entity this message applies to |






<a name="charitysuite.DeleteMemberResponse"></a>

### DeleteMemberResponse
Response from delete a MemberEntity


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| member_ref | [OrgEntityReference](#charitysuite.OrgEntityReference) |  | Reference to the entity this message applies to |






<a name="charitysuite.GetMemberRequest"></a>

### GetMemberRequest
Request to get the information of a member of an organisation


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| member_ref | [OrgEntityReference](#charitysuite.OrgEntityReference) |  | Reference to the entity this message applies to |






<a name="charitysuite.GetMemberResponse"></a>

### GetMemberResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| entity | [MemberEntity](#charitysuite.MemberEntity) |  | Reference to the entity this message applies to |






<a name="charitysuite.ListMembersRequest"></a>

### ListMembersRequest
Request to list members of an organisation


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_ref | [Reference](#charitysuite.Reference) |  | Reference to the organisation this message applies to |






<a name="charitysuite.ListMembersResponse"></a>

### ListMembersResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| entity | [MemberEntity](#charitysuite.MemberEntity) |  | Reference to the entity this message applies to |






<a name="charitysuite.UpdateMemberRequest"></a>

### UpdateMemberRequest
Request to update a MemberEntity


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| member_ref | [OrgEntityReference](#charitysuite.OrgEntityReference) |  | Reference to the entity this message applies to |
| update_contact_request | [UpdateMemberRequest.UpdateContactRequest](#charitysuite.UpdateMemberRequest.UpdateContactRequest) |  | Field to hold a request to update the contact |
| update_address_request | [UpdateMemberRequest.UpdateAddressRequest](#charitysuite.UpdateMemberRequest.UpdateAddressRequest) |  | Field to hold a request to update the address |
| update_roles_request | [UpdateMemberRequest.UpdateMemberRolesRequest](#charitysuite.UpdateMemberRequest.UpdateMemberRolesRequest) |  | Field to hold a request to update the roles |
| update_pin_request | [UpdateMemberRequest.UpdatePINRequest](#charitysuite.UpdateMemberRequest.UpdatePINRequest) |  | Field to hold a request to update the roles |
| update_password_request | [UpdateMemberRequest.UpdateEmailPasswordRequest](#charitysuite.UpdateMemberRequest.UpdateEmailPasswordRequest) |  | Field to hold a request to update the roles |






<a name="charitysuite.UpdateMemberRequest.UpdateAddressRequest"></a>

### UpdateMemberRequest.UpdateAddressRequest
Message to hold a request to update the address


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| address | [AddressValue](#charitysuite.AddressValue) |  | Address details - provide value to set, leave blank to clear |






<a name="charitysuite.UpdateMemberRequest.UpdateContactRequest"></a>

### UpdateMemberRequest.UpdateContactRequest
Message to hold a request to update the contact


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| contact | [ContactValue](#charitysuite.ContactValue) |  | Contact details - provide value to set, leave blank to clear |






<a name="charitysuite.UpdateMemberRequest.UpdateEmailPasswordRequest"></a>

### UpdateMemberRequest.UpdateEmailPasswordRequest
Message to hold a request to update the password for a Member that has an email/password login account.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| password | [string](#string) |  | New password for this member |






<a name="charitysuite.UpdateMemberRequest.UpdateMemberRolesRequest"></a>

### UpdateMemberRequest.UpdateMemberRolesRequest
Message to hold a request to update the roles


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| roles | [MemberRole](#charitysuite.MemberRole) | repeated | Roles for this Member |






<a name="charitysuite.UpdateMemberRequest.UpdatePINRequest"></a>

### UpdateMemberRequest.UpdatePINRequest
Message to hold a request to update the PIN for Till Point login.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| tillLoginPIN | [string](#string) |  | Roles for this Member |






<a name="charitysuite.UpdateMemberResponse"></a>

### UpdateMemberResponse
Response from update a MemberEntity


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| member_ref | [OrgEntityReference](#charitysuite.OrgEntityReference) |  | Reference to the entity this message applies to |





 

 

 


<a name="charitysuite.MemberService"></a>

### MemberService
The MemberService is used to manage members of the organisation.

A Member is a participant in the organisation that can have an associated UserAccount for logging in
to the CharitySuite system.

A Member can also be assigned a set of _MemberRoles_ determining their access level within CharitySuite.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| ListMembers | [ListMembersRequest](#charitysuite.ListMembersRequest) | [ListMembersResponse](#charitysuite.ListMembersResponse) stream | Returns a stream of all of the MemberEntity objects within the requested Org |
| GetMember | [GetMemberRequest](#charitysuite.GetMemberRequest) | [GetMemberResponse](#charitysuite.GetMemberResponse) | Returns the requested MemberEntity identified but the input reference. |
| CreateMember | [CreateMemberRequest](#charitysuite.CreateMemberRequest) | [CreateMemberResponse](#charitysuite.CreateMemberResponse) | Create a new MemberEntity |
| UpdateMember | [UpdateMemberRequest](#charitysuite.UpdateMemberRequest) | [UpdateMemberResponse](#charitysuite.UpdateMemberResponse) | Create an existing MemberEntity |
| DeleteMember | [DeleteMemberRequest](#charitysuite.DeleteMemberRequest) | [DeleteMemberResponse](#charitysuite.DeleteMemberResponse) | Delete an existing MemberEntity |

 



<a name="charitysuite/organisation_entity.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/organisation_entity.proto



<a name="charitysuite.OrganisationEntity"></a>

### OrganisationEntity
An organisation.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_id | [EntityId](#charitysuite.EntityId) |  | EntityId of the containing organisation |
| entity_id | [EntityId](#charitysuite.EntityId) |  | EntityId of this instance within its type (for this record - same as org_id) |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| contact | [ContactValue](#charitysuite.ContactValue) |  | Contact Details |
| address | [AddressValue](#charitysuite.AddressValue) |  | Address Details |





 

 

 

 



<a name="charitysuite/organisation_service.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/organisation_service.proto
MemberService and related message types.


<a name="charitysuite.ListAreasRequest"></a>

### ListAreasRequest
Request to list members of an organisation


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_ref | [Reference](#charitysuite.Reference) |  | Reference to the organisation this message applies to |






<a name="charitysuite.ListAreasResponse"></a>

### ListAreasResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| entity | [AreaEntity](#charitysuite.AreaEntity) |  | Reference to the entity this message applies to |






<a name="charitysuite.ListLocationsRequest"></a>

### ListLocationsRequest
Request to list members of an organisation


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_ref | [Reference](#charitysuite.Reference) |  | Reference to the organisation this message applies to |






<a name="charitysuite.ListLocationsResponse"></a>

### ListLocationsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| entity | [LocationEntity](#charitysuite.LocationEntity) |  | Reference to the entity this message applies to |






<a name="charitysuite.ListOrganisationsRequest"></a>

### ListOrganisationsRequest
Request to list members of an organisation






<a name="charitysuite.ListOrganisationsResponse"></a>

### ListOrganisationsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| entity | [OrganisationEntity](#charitysuite.OrganisationEntity) |  | Reference to the entity this message applies to |






<a name="charitysuite.ListRegionsRequest"></a>

### ListRegionsRequest
Request to list members of an organisation


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_ref | [Reference](#charitysuite.Reference) |  | Reference to the organisation this message applies to |






<a name="charitysuite.ListRegionsResponse"></a>

### ListRegionsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| entity | [RegionEntity](#charitysuite.RegionEntity) |  | Reference to the entity this message applies to |





 

 

 


<a name="charitysuite.OrganisationService"></a>

### OrganisationService
The MemberService is used to manage members of the organisation.

A Member is a participant in the organisation that can have an associated UserAccount for logging in
to the CharitySuite system.

A Member can also be assigned a set of _MemberRoles_ determining their access level within CharitySuite.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| ListOrganisations | [ListOrganisationsRequest](#charitysuite.ListOrganisationsRequest) | [ListOrganisationsResponse](#charitysuite.ListOrganisationsResponse) stream | Returns a stream of all of the MemberEntity objects within the requested Org |
| ListRegions | [ListRegionsRequest](#charitysuite.ListRegionsRequest) | [ListRegionsResponse](#charitysuite.ListRegionsResponse) stream | Returns a stream of all of the MemberEntity objects within the requested Org |
| ListAreas | [ListAreasRequest](#charitysuite.ListAreasRequest) | [ListAreasResponse](#charitysuite.ListAreasResponse) stream | Returns a stream of all of the MemberEntity objects within the requested Org |
| ListLocations | [ListLocationsRequest](#charitysuite.ListLocationsRequest) | [ListLocationsResponse](#charitysuite.ListLocationsResponse) stream | Returns a stream of all of the MemberEntity objects within the requested Org |

 



<a name="charitysuite/references.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/references.proto
Reference Types


<a name="charitysuite.OrgEntityReference"></a>

### OrgEntityReference
A reference to an Entity within an Organisation


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_ref | [Reference](#charitysuite.Reference) |  | Reference to the organisation that contains this Entity |
| entity_ref | [Reference](#charitysuite.Reference) |  | Reference to the Entity |






<a name="charitysuite.Reference"></a>

### Reference
A Reference is a pointer to a thing, usually an Entity type that has appropriate identifiers.

References can include an internal XOR an external id.

*External Ids*

When an Entity is created it can be assigned an external id by the creating party.
This must be unique within the that Entity type (for an Organisation).

*Internal Ids*

When an Entity is created it will be assigned an internal identifier.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| internal_id | [string](#string) |  | Internally assigned identifier |
| external_id | [string](#string) |  | Externally provided identifier |





 

 

 

 



<a name="charitysuite/region_entity.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/region_entity.proto



<a name="charitysuite.RegionEntity"></a>

### RegionEntity
A region.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| org_id | [EntityId](#charitysuite.EntityId) |  | EntityId of the containing organisation |
| entity_id | [EntityId](#charitysuite.EntityId) |  | EntityId of this instance within its type (for this record - same as org_id) |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| contact | [ContactValue](#charitysuite.ContactValue) |  | Contact Details |
| address | [AddressValue](#charitysuite.AddressValue) |  | Address Details |





 

 

 

 



<a name="charitysuite/status_service.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/status_service.proto



<a name="charitysuite.GetStatusRequest"></a>

### GetStatusRequest
GetStatus request parameters






<a name="charitysuite.GetStatusResponse"></a>

### GetStatusResponse
GetStatus Response


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [string](#string) |  | Simple string status value, e.g. &#39;OK&#39; |





 

 

 


<a name="charitysuite.StatusService"></a>

### StatusService
The StatusService is a Work In Progress.

This service will provide access points for checking the health and availability of the this application.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetStatus | [GetStatusRequest](#charitysuite.GetStatusRequest) | [GetStatusResponse](#charitysuite.GetStatusResponse) | Basic status request |

 



<a name="charitysuite/user_account_value.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## charitysuite/user_account_value.proto



<a name="charitysuite.UserAccountValue"></a>

### UserAccountValue
Describes a user account


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| user_id | [string](#string) |  | The user identifier given to this account |
| email | [string](#string) |  | The email address associated with this account |
| email_verified | [bool](#bool) |  | Whether the email address has been verified |
| provider_id | [ProviderId](#charitysuite.ProviderId) |  | The Provider for this authentication identity |





 


<a name="charitysuite.ProviderId"></a>

### ProviderId
Authentication providers

| Name | Number | Description |
| ---- | ------ | ----------- |
| PROVIDER_UNDEFINED | 0 | Undefined value |
| PROVIDER_PASSWORD | 1 | Email/Password login provider |
| PROVIDER_MICROSOFT | 2 | Microsoft and Active Directory login accounts |
| PROVIDER_GOOGLE | 3 | Google login accounts |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |


CharitySuite Integration Service API
====================================

[gRPC Protobuf Files](protos)

[gRPC Generated API Docs](grpc-api.md)

The Integration Service is designed to be used by 3rd Party systems wanting to connect their IT systems to CharitySuite.
This is not a user focused service.


Connecting to the Service
-------------------------

This assumes that you are an existing [CharitySuite](https://micromkt.co.uk) customer.

To connect to the service you will need:

1. A service account certificate file. (Provided by MicroMkt)
1. An API Key. (Provided by MicroMkt)
1. A gRPC client written in the language of your choice using the `*.proto` files from this repo.
1. Connectivity details (host:port) for the service (Provided by MicroMkt)
1. The ability to generate a google IDTOKEN from the service account file.
   There are various options for doing this, e.g. programmatically via Google SDKs or via [gcloud command line](https://cloud.google.com/sdk/gcloud/reference/auth/print-identity-token)  


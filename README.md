CharitySuite Integration Service Documentation
==============================================

The _CharitySuite Integration Service_ provides an API to allow integration with the CharitySuite Application.

This documentation is for existing [CharitySuite](https://micromkt.co.uk) customers.

[View API Docs](api)


Error Responses
===============

The service uses standard GRPC Error response codes and messages to return errors. 
The codes used by the service are listed below and where relevant their mapping to equivalent HTTP codes. 

```typescript
export const GRPCResponseCodes = {
  OK: {
    grpcCode: 0,
    httpCode: '200',
  },
  CANCELLED: {
    grpcCode: 1,
    httpCode: null,
  },
  UNKNOWN: {
    grpcCode: 2,
    httpCode: null,
  },
  ALREADY_EXISTS: {
    grpcCode: 6,
    httpCode: null,
  },
  INVALID_ARGUMENT: {
    grpcCode: 4,
    httpCode: '400',
  },
  NOT_FOUND: {
    grpcCode: 5,
    httpCode: '404',
  },
  PERMISSION_DENIED: {
    grpcCode: 7,
    httpCode: '403',
  },
  UNIMPLEMENTED: {
    grpcCode: 12,
    httpCode: '501',
  },
  INTERNAL: {
    grpcCode: 13,
    httpCode: '500',
  },
  UNAUTHENTICATED: {
    grpcCode: 16,
    httpCode: '401',
  },
};
```
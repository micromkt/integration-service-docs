import {JWTInput} from 'google-auth-library';
import * as dotenv from 'dotenv';
import {JWT} from 'google-auth-library';
import path = require('path');
import {readFileSync} from 'fs';
import {apiGatewayClientArgs} from './credentials';
import {testEchoService} from './testEchoService';

console.log('grcp client app runing\n\n');

// Load in the environment variables as defined in the .env file
const result = dotenv.config();
if (result.error) {
  throw result.error;
}

export const CHARITYSUITE_AUDIENCE = 'https://charitysuite.uk';
const HOST = `${process.env.INTEGRATION_SERVICE_GATEWAY_HOST}:${process.env.INTEGRATION_SERVICE_API_GATEWAY_PORT}`;

/**
 *
 * @returns Read amd parse in a google credential file provided by MicroMkt
 */
export const getJwtFromGACFile = async (): Promise<JWTInput> => {
  const keyFile = path.resolve(
    process.env.GOOGLE_APPLICATION_CREDENTIALS || ''
  );
  // console.log(keyFile);
  return await JSON.parse(readFileSync(keyFile).toString());
};

/**
 * Create and sign a new jwt token based on google credential files.
 * @returns Signed jwt token
 */
export const signJwtToken = async () => {
  const jwt = await getJwtFromGACFile();
  const client = new JWT();
  await client.fromJSON(jwt as JWTInput);

  // fetch an id token using the audiance url
  const idToken = await client.fetchIdToken(CHARITYSUITE_AUDIENCE);
  console.log(`fetchIdToken: \n ${idToken}\n`);

  return idToken;
};

/**
 * Make an example request to a mock method for testing the integration service provided by MicroMkt.
 * This service is running in a live environment and is reprentative of the
 * integration service.
 *
 * This authenticates api calls using GoogleAuth client and passing
 * the CHARITYSUITE_AUDIENCE url to fetch an id token
 */
const makeGrpcCallUsingDefaultCred = async () => {
  const gatewayArgs = apiGatewayClientArgs(HOST, process.env.API_KEY!);
  const argp = () => gatewayArgs;

  testEchoService(argp)
    .then(() => console.log('DONE'))
    .catch(err => console.log(err));
};

async function main() {
  // Print out a valid jwt token
  // console.log(await signJwtToken());

  // Run the test echo service using the host and API Key provided by MicroMkt
  makeGrpcCallUsingDefaultCred();
}

main().catch(console.error);

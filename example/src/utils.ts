export const also = <T>(obj: T, body: (it: T) => void) => {
    body(obj);
    return obj;
};
import {GoogleAuth} from 'google-auth-library';
import * as grpc from 'grpc';
import {JWT} from 'google-auth-library/build/src/auth/jwtclient';
import {ClientArgs} from './types';
import {also} from './utils';

export const CHARITYSUITE_AUDIENCE = 'https://charitysuite.uk';

/**
 * Factory method to create CallCredentials
 *
 * Specify aud override otherwise service URL is used.
 */
const googleIdTokenCredential = async (aud?: string) => {
  // Create a Google auth using Default Credentials
  const googleAuth = new GoogleAuth();

  return grpc.credentials.createFromMetadataGenerator(
    async ({service_url}, cb) => {
      const audience = aud ?? service_url;

      // Create an Auth client (should return JWT)
      const gaclient = (await googleAuth.getClient()) as JWT;

      // Fetch an idtoken and write into Bearer
      gaclient
        .fetchIdToken(audience)
        .then(token => {
          console.log('token', token);
          cb(
            null,
            also(new grpc.Metadata(), m => {
              m.add('Authorization', 'Bearer ' + token);
            })
          );
        })
        .catch(cb);
    }
  );
};

/**
 * Factory method to create CallCredentials using api key
 * @param apiKey
 */
const apiKeyCallCredentials = (apiKey: string) => {
  return grpc.credentials.createFromMetadataGenerator(async (_, cb) => {
    cb(
      null,
      also(new grpc.Metadata(), m => {
        m.add('x-api-key', apiKey);
      })
    );
  });
};

const channelCredentialsForGCPAPIGateway = async (apiKey: string) =>
  grpc.credentials
    .createSsl()
    .compose(await googleIdTokenCredential(CHARITYSUITE_AUDIENCE))
    .compose(await apiKeyCallCredentials(apiKey));

const channelCredentialsForLocalTesting = async () =>
  grpc.credentials.createInsecure();

export const apiGatewayClientArgs = async (host: string, apiKey: string) =>
  [host, await channelCredentialsForGCPAPIGateway(apiKey)] as ClientArgs;

export const localTestingClientArgs = async (host: string) =>
  [host, await channelCredentialsForLocalTesting()] as ClientArgs;

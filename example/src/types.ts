import * as grpc from 'grpc';

export type ClientArgs = [
  address: string,
  credentials: grpc.ChannelCredentials,
  options?: object
];

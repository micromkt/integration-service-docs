import {also} from './utils';
import * as grpc from 'grpc';
import {EchoServiceClient} from './generated/charitysuite/echo_service_grpc_pb';
import {
  EchoCredentialsRequest,
  EchoCredentialsResponse,
  EchoGatherResponse,
  EchoMessage,
  EchoNRequestMessage,
} from './generated/charitysuite/echo_service_pb';

const newEchoMessage = (obj: EchoMessage.AsObject) =>
  also(new EchoMessage(), it => {
    it.setText(obj.text);
  });

export class EchoServiceStub {
  constructor(
    readonly client: EchoServiceClient,
    readonly metaDataSupplier = () => new grpc.Metadata()
  ) {}

  async echoSingle(obj: EchoMessage.AsObject): Promise<EchoMessage.AsObject> {
    return new Promise((resolve, reject) => {
      const call = this.client.echoSingle(
        newEchoMessage(obj),
        this.metaDataSupplier(),
        (err, res) => {
          if (err) reject(err);
          else resolve(res.toObject());
        }
      );
    });
  }

  async echoN(
    msg: EchoMessage.AsObject,
    nos: number
  ): Promise<EchoMessage.AsObject[]> {
    const rstream = this.client.echoN(
      also(new EchoNRequestMessage(), it => {
        it.setMessage(newEchoMessage(msg));
        it.setNumberOfMessages(nos);
      })
    );

    const msgs: EchoMessage.AsObject[] = [];
    for await (const chunk of rstream) {
      msgs.push((chunk as EchoMessage).toObject());
    }

    return msgs;
  }

  async echoGather(
    ...msgs: EchoMessage.AsObject[]
  ): Promise<EchoGatherResponse.AsObject> {
    return new Promise((resolve, reject) => {
      const wstream = this.client.echoGather(
        this.metaDataSupplier(),
        (err, res) => {
          if (err) reject(err);
          else resolve(res.toObject());
        }
      );

      msgs.forEach(msg => wstream.write(newEchoMessage(msg)));
      wstream.end();
    });
  }

  async echoBidi(
    ...objs: EchoMessage.AsObject[]
  ): Promise<EchoMessage.AsObject[]> {
    const duplex = this.client.echoBidi();

    objs.forEach(msg => duplex.write(newEchoMessage(msg)));
    duplex.end();

    const msgs: EchoMessage.AsObject[] = [];
    for await (const chunk of duplex) {
      msgs.push((chunk as EchoMessage).toObject());
    }

    return msgs;
  }

  async echoCredentials(): Promise<EchoCredentialsResponse.AsObject> {
    return new Promise((resolve, reject) => {
      const call = this.client.echoCredentials(
        new EchoCredentialsRequest(),
        this.metaDataSupplier(),
        (err, res) => {
          if (err) reject(err);
          else resolve(res.toObject());
        }
      );
    });
  }
}

import {EchoServiceStub} from './EchoServiceStub';
import {EchoServiceClient} from './generated/charitysuite/echo_service_grpc_pb';
import {ClientArgs} from './types';

export async function testEchoService(
  clientArgsProvider: () => Promise<ClientArgs>
) {
  const args = await clientArgsProvider();
  const echoServiceStub = new EchoServiceStub(new EchoServiceClient(...args));

  console.log(`
  EchoService: Calling echoSingle
  ===============================
  `);
  const echoSingleResult = await echoServiceStub.echoSingle({
    text: 'echoSingle: Message in a bottle',
  });
  console.log(echoSingleResult);

  console.log(`
  EchoService: Calling echoN
  ==========================
  `);
  const echoNResult = await echoServiceStub.echoN(
    {
      text: 'echoN: Message in a bottle',
    },
    15
  );
  console.log(echoNResult);

  console.log(`
  EchoService: Calling echoGather
  ===============================
  `);
  const echoGatherResult = await echoServiceStub.echoGather(
    ...[...Array(5)].map((_, i) => ({
      text: 'echoGather: Message in a can ' + i,
    }))
  );
  console.log(echoGatherResult);

  console.log(`
  EchoService: Calling echoBidi
  =============================
  `);
  const echoBidiResult = await echoServiceStub.echoBidi(
    ...[...Array(5)].map((_, i) => ({
      text: 'echoBidi: Message in a can ' + i,
    }))
  );
  console.log(echoBidiResult);
  console.log(`

  EchoService: Calling echoCredentials
  ====================================
  `);
  const echoCredentialsResult = await echoServiceStub.echoCredentials();
  console.log(echoCredentialsResult);
}

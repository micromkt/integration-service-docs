# CharitySuite Integration Service Client Example

Example project of a gRPC client consuming the Google authentication library and the CharitySuite Integration Service API.

## Description

The purpose of this project is to create an example client consuming the CharitySuite Integration Service API and to document the process.

Using the proto files and the [protoc compiler](https://developers.google.com/protocol-buffers) provided by Google this client can be written in a framework and language of your choosing. This documentation is a guide for MicroMkt customers into implementing a client side app using vanilla Node v12@latest in TypeScript.

For a detailed explanation of how this client is implemented a step by step development guide is available below.

## Project Structure and Setup

Description of the directory structure and purpose of each folder for this project.

`Keys`: _untracked folder containing the service account certificate file provided by MicroMkt._

`.env`: _untracked file containing private environment variables._

`src`: _directory containing the source code for the client and generated proto files._

`src/generated`: _untracked folder containing all of the generated .js and .ts files using the protobuf files._

`src/index.ts`: _entry point for the client._

`src/credentials`: _contains helper methods to create a grcp client with jwt bearer token._

While it is not necessary to untrack the `src/proto` and `src/generated` directories, this project treats the provided CharitySuite Integration Service API as a single source of truth for the API definition. The proto files are copied in using the `copy-protos-from-docs-repo.sh` script and generate client interface using `npm run gen:grpc`.

## Managing Environment Variables and Secrets

Environment variables and secrets should not be tracked by git or any other version control software. Refer to the `.gitignore` of this project to see what files and folders are untracked.

### Environment Variables

This project uses `dotenv` to load in environment variables at runtime. A `.env.example` file is provided in the root directory that contains a template of what the `.env` should look like. Place the provided MicroMkt environment variables into the appropriate key value pairs.

This file should not be tracked by git. Refer to the dotenv documentation for best practices on keeping secrets, how to set up and load a `.env` file at runtime.

### Secrets

A `keys` folder that is not tracked by git contains the service account certificate file provided by MicroMkt. The `GOOGLE_APPLICATION_CREDENTIALS` should point to this file.

If you have chosen to generate an identity token using the gcloud cli, save the identity token inside the keys folder as a `.pem` file. Pass this identity token into the grcp metadata class to authorise API calls.

## Step by Step Development Guide

1. Started an empty node TS project using [this google boilerplate script](https://github.com/google/gts). This client project is then built up from the boilerplate node-ts client, it’s not necessary to run this command when using the provided example client.

       ```bash
       npx gts init
       ```

2. Imported environment variables and credentials provided by MicroMkt as described in the CharitySuite Integration. Service API. Private keyfiles and environment variables should be untracked by git.
   
    2.1 Placed the service account certificate file inside the untracked `keys` folder.
   
    2.2 Rename `.env.example` to `.env`
   
    2.3 Set the provided API Key in the `.env` file.
   
    2.4 Set the provided connectivity details for the Integration Service API in the `.env` file.

3. Generate interfaces using the provided `.proto` files provided by MicroMkt. These files can be compiled into a language of your choice as described by the Google protobuf documentation [here](https://developers.google.com/protocol-buffers).

    3.1 Run `npm run gen:grpc:grpc-js` to generate `.js` files.

    3.2 Run `npm run gen:grpc:ts-types` to generate `.ts` files.

    3.3 Run `npm run gen:grp` to generate both `.js` and `.ts` files.

    3.4 To generate stub files in another language refer to the [Google Protobuf documentation](https://developers.google.com/protocol-buffers) and compile the protobuf files using the `protoc` command.

    3.5 This example uses both first and third party libraries to compile `.proto` files into `.js` and `.ts`.

4. Generate a [google identity-token](https://cloud.google.com/sdk/gcloud/reference/auth/print-identity-token) from the provided service account file.

     4.1 gcloud cli example usage (Note: must have [gcloud cli](https://cloud.google.com/sdk/docs/install#mac)) installed). In the root directory of the project run the following:

      ```bash
      # Authorize access to Google Cloud Platform with a service account using a key file
      gcloud auth activate-service-account --key-file={path/to/keyfile}key.json

      # Generate and print an identity token for the specified account
      gcloud auth print-identity-token
      ```

     4.2 Copy the identity token from the console and place it into the keys folder as `.pem` file.

     4.3 Using JSON Web Tokens or the provided service credentials file from google-auth-library. An example is provided in `src/index` to generate a JWT token containing an identity token.

     4.4 Use [jwt.io](https://jwt.io/) debug to verify the token payload. JWT.io also provides a list of libraries for token signing/verification for various languages.

5. Writing the client side to consume the compiled protobuf files and CharitySuite Integration Service API.

    5.1 See `src/index.ts` to see how the protobuf files are being consumed by the client to test against the echo service. This echo service is running in a live cloud environment that is representative of the integration service server.

    5.2 The test echo example can be used to mock out client implementations and receive real results. This example focused on passing a JWT with a google identity token to the grpc client metadata to authorise calls.

    5.3 Alternative libraries are available for signing and authorising a JWT outside the Google ecosystem as [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken).

## Run This Example

This project uses [ts-node](https://github.com/TypeStrong/ts-node) to run the client side app.

First install the dependencies.

```bash
npm i
```

Generate the protbuf files.

```bash
npm run gen:grcp
```


Then to run the example project (Assuming the environment variables have been set correctly).

```bash
npm run start
```

## References

- [Google Authenticating service-to-service](https://cloud.google.com/run/docs/authenticating/service-to-service)
- [Using OAuth 2.0 for Server to Server Applications](https://developers.google.com/identity/protocols/oauth2/service-account)
- [gcloud auth print-identity-token](https://cloud.google.com/sdk/gcloud/reference/auth/print-identity-token)
- [gcloud auth activate-service-account](https://cloud.google.com/sdk/gcloud/reference/auth/activate-service-account)
- [JWT.io](https://jwt.io/)
- [dotenv](https://www.npmjs.com/package/dotenv)
- [Protocol buffers](https://developers.google.com/protocol-buffers)
- [Installing Google Cloud SDK](https://cloud.google.com/sdk/docs/install)
- [Google-auth-library](https://www.npmjs.com/package/google-auth-library)
- [Authentication between services | Cloud Endpoints with gRPC](https://cloud.google.com/endpoints/docs/grpc/service-account-authentication#calling_authenticated_methods_from_a_grpc_client)
- [Why and when to use API keys](https://cloud.google.com/endpoints/docs/grpc/when-why-api-key)
- [Authenticating as a service account | Authentication](https://cloud.google.com/docs/authentication/production#auth-cloud-explicit-csharp)
- [Using JSON Web Tokens (JWTs)](https://cloud.google.com/iot/docs/how-tos/credentials/jwts)
- [Authentication overview](https://cloud.google.com/docs/authentication)
